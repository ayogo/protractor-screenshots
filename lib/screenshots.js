(function(globals, jasmine) {
  if (!globals) {
    throw new Error("Couldn't detect globals!");
  }

  var fs        = require('fs');
  var mkdirp    = require('mkdirp');
  var slug      = require('slug');
  var Q         = require('q');
  var _         = require('lodash');

  var capabilities = null;
  browser.getCapabilities().then(function(returnValue) {
    return capabilities = returnValue.caps_;
  });

  var disableScreenshots = browser.params['disableScreenshots'];
  var screenshotBase = browser.params['screenshotsBasePath'] || '.';
  var screenshotSizes = browser.params['screenshotSizes'];

  function matchesCapabilities(other) {
    var excludeKeys;
    excludeKeys = ['sizes'];
    return _.every(other, function(value, key) {
      if (excludeKeys.indexOf(key) !== -1) {
        return true;
      }
      return capabilities[key] === value;
    });
  }

  function saveScreenshot(screenshotName, screenshot) {
    var filename, path, writeImage;
    path = "" + screenshotBase + "/" + screenshot.type;
    if (screenshot.dir) {
      path += "/" + screenshot.dir;
    }
    filename = "" + (slug(screenshotName)) + ".png";
    writeImage = function(data) {
      return Q.nfcall(mkdirp, path).then(function() {
        return Q.nfcall(fs.writeFile, "" + path + "/" + filename, data, {
          encoding: 'base64'
        });
      });
    };
    return Q.fcall(writeImage, screenshot.data);
  }

  function takeScreenshot(screenshotName, screenshotDir, delay, spec) {
    if (capabilities.browserName == "firefox") {
      browser.executeScript("window.scrollTo(0,0);");
    }
    delay = (delay | 0) || 100;
    function setScreenSize(width, height) {
      return browser.driver.manage().window().setSize(width, height);
    };

    var screenSizes, _ref;
    if (_ref = _.find(screenshotSizes, matchesCapabilities)) {
        screenSizes = _ref.sizes;
    }

    function actualTakeScreenshot(type, size) {
      return browser.driver.manage().window().getSize().then(function(screenSize) {
        return browser.takeScreenshot().then(function(data) {
          return saveScreenshot(screenshotName, {
            data: data,
            dir: screenshotDir,
            type: type,
            size: size
          });
        });
      });
    };

    var res;
    if (spec && spec.suite && spec.suite.hasOwnProperty('_screenshotSize')) {
      res = spec.suite._screenshotSize;
    }

    if (Object.keys(screenSizes || {}).length > 0) {
      return browser.driver.manage().window().getSize().then(function(preSize) {
        var pSizes = [];

        Object.keys(screenSizes).forEach(function(type) {
          var size = screenSizes[type];
          var p;
          var tags = size.tags || [];

          if (res && res != type && tags.indexOf(res.toLowerCase()) === -1) {
            return;
          }

          var fn = function() {
            browser.sleep(delay);

            return actualTakeScreenshot(type, size);
          };

          if (preSize.width != size.width && preSize.height != size.height) {
            p = setScreenSize(size.width, size.height).then(fn);
          } else {
            p = fn();
          }

          pSizes.push(p);
        });

        return Q.all(pSizes).then(function() {
          return setScreenSize(preSize.width, preSize.height);
        });
      });
    }
  }



  /* Public API */

  if (jasmine) {
    jasmine.Env.prototype.withResolution = function(size, specDef) {
      var suite = this.describe('on ' + size, specDef);

      suite._screenshotSize = size;

      var screenshotSizes = browser.params['screenshotSizes'];

      var screenSizes, _ref;
      if (_ref = _.find(screenshotSizes, matchesCapabilities)) {
          screenSizes = _ref.sizes;
      }

      var ACTUAL_SIZE = screenSizes[size];

      if (!ACTUAL_SIZE) {
        Object.keys(screenSizes).forEach(function(type) {
          var ssize = screenSizes[type];
          var tags = ssize.tags || [];

          if (size && size != type && tags.indexOf(size.toLowerCase()) === -1) {
            return;
          }

          if (!ACTUAL_SIZE) {
            ACTUAL_SIZE = ssize;
          }
        });
      }

      if (ACTUAL_SIZE) {
        var preSize;

        suite.beforeEach(function() {
          return browser.driver.manage().window().getSize().then(function(sz) {
            preSize = sz;
          }).then(function() {
            return browser.driver.manage().window().setSize(ACTUAL_SIZE.width, ACTUAL_SIZE.height);
          });
        });

        suite.afterEach(function() {
          return browser.driver.manage().window().setSize(preSize.width, preSize.height);
        });
      }

      return suite;
    };

    globals.withResolution = function(size, specDef) {
      jasmine.getEnv().withResolution(size, specDef);
    };

    globals.takeScreenshot = function(screenshotName, screenshotDir, delay) {
      if (disableScreenshots) {
        return;
      }

      var env = jasmine.getEnv();
      if (env.currentSpec) {
        takeScreenshot(screenshotName, screenshotDir, delay, env.currentSpec);
      }
    };
  }

  exports.takeScreenshot = function(screenshotName, screenshotDir, delay) {
    if (disableScreenshots) {
      return;
    }
    return takeScreenshot(screenshotName, screenshotDir, delay);
  };
})(
  (typeof window == 'object' && window) || (typeof global == 'object' && global),
  jasmine
);
