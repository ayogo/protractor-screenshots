fs = require 'fs'
mkdirp = require 'mkdirp'
slug = require 'slug'
Q = require 'q'
_ = require 'lodash'

capabilities = null
browser.getCapabilities().then (returnValue) ->
    capabilities = returnValue.caps_

disableScreenshots = browser.params['disableScreenshots']
screenshotBase = browser.params['screenshotsBasePath'] || '.'
screenshotSizes = browser.params['screenshotSizes']

matchesCapabilities = (other) ->
    excludeKeys = ['sizes']

    return _.every other, (value, key) ->
        if excludeKeys.indexOf(key) != -1
            return true

        return capabilities[key] == value

saveScreenshot = (screenshotName, screenshot) ->
    path =  "#{screenshotBase}/#{screenshot.type}"

    if screenshot.dir
        path += "/#{screenshot.dir}"

    filename = "#{slug(screenshotName)}.png"

    writeImage = (data) ->
        return Q.nfcall(mkdirp, path).then () ->
            return Q.nfcall(
                fs.writeFile,
                "#{path}/#{filename}",
                data,
                { encoding: 'base64'}
            )

    return Q.nfcall(writeImage, screenshot.data)

takeScreenshot = (screenshotName, screenshotDir) ->
    setScreenSize = (width, height) ->
        return browser.driver.manage().window().setSize(width, height)


    screenSizes = _.find(screenshotSizes, matchesCapabilities)?.sizes

    actualTakeScreenshot = (type, size) ->
        browser.driver.manage().window().getSize()
        .then (screenSize) ->
            browser.takeScreenshot()
            .then (data) ->
                saveScreenshot(screenshotName, {data: data, dir: screenshotDir, type: type, size: size})

    if Object.keys(screenSizes || {}).length > 0
        Object.keys(screenSizes).reduce (soFar, type) ->
            size = screenSizes[type]
            soFar.then(
                setScreenSize(size.width, size.height)
                .then () ->
                    return actualTakeScreenshot(type, size)
            )
        , Q(true)

###
Public API
###

exports.takeScreenshot = (screenshotName, screenshotDir) ->
    if disableScreenshots
        return

    return takeScreenshot(screenshotName, screenshotDir)
